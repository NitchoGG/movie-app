import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Messages extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            messages : [{
                messageTitle : "Fix message siden",
                messageText : "Hej vil du ikke være sød og få fixed message siden, vi skal bruge den til imorgen",
                senderImage : "https://pixel.nymag.com/imgs/daily/vulture/2015/05/01/01-robert-downey-jr-indie.w330.h330.jpg"
            },
                {
                    messageTitle : "Fix message siden",
                    messageText : "Hej vil du ikke være sød og få fixed message siden, vi skal bruge den til imorgen",
                    senderImage : "https://m.media-amazon.com/images/M/MV5BNzg1MTUyNDYxOF5BMl5BanBnXkFtZTgwNTQ4MTE2MjE@._V1_.jpg"
                },
                {
                    messageTitle : "Fix message siden",
                    messageText : "Hej vil du ikke være sød og få fixed message siden, vi skal bruge den til imorgen",
                    senderImage : "https://www.hindustantimes.com/rf/image_size_960x540/HT/p2/2018/04/03/Pictures/_56db43e2-3722-11e8-8aa5-05fdb8d0ae52.jpg"
                }]
        }

    }

    componentWillMount() {
        if (!this.props.auth.loggedIn()) this.props.history.push('/Home');
    }

    render() {
        let messages = this.state.messages.map((item) =>
            <div className={"row messageContainer"}>
                <div className={"col-12"}>
                    <div className={"messageImg"}>
                    <img
                        src={item.senderImage}/>
                    </div>
                    <div className={"col-10 message"}>
                        <h4 className={"messageTitle"}>{item.messageTitle}</h4>
                        <p className={"messageText"}>{item.messageText}</p>
                    </div>
                </div>
            </div>
        );

        return (
            <div className="container-fluid ">
                {messages}
            </div>
        );
    }
}