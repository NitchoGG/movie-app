import React, {Component} from 'react';
import Rating from "./Rating";
import {BrowserRouter as Router, Switch, Route, Link, NavLink} from 'react-router-dom';
import Slider from './slider';

export default class Home extends Component {

    constructor(props) {
        super(props);
    }

    handleTagClick = (e) => {
        e.preventDefault();
        this.props.getMoviesByTag(e.target.id);
    }

    render() {
        let tagList = [];
        let tags = [];
        let tmp = [];
        let movies = [];

        if (this.props.movies)
        {
            this.props.movies.forEach((movie, index) =>
                movies.push(<Link key={index} to={"/Movie/" + movie.id} className="movielink">
                    <img alt="poster" src={'https://image.tmdb.org/t/p/w300_and_h450_bestv2' + movie.cover} />
                </Link>)
            )
        }

        if (this.props.tags)
        {
            this.props.tags.forEach((tag, index) => {
                tags.push(<li key={index}><a className={"genreTag"} id={tag.genreId} onClick={this.handleTagClick} href="#">{tag.genre}</a></li>)
            })
        }

        if(this.props.currentMovie)
        {
            if (this.props.currentMovie.length > 0)
            {
                tmp = this.props.currentMovie[0];
            }
            else
            {
                tmp = this.props.currentMovie;
            }

            tagList = tmp.tags.map((item, index) =>
                <p key={index} className="movieTags">{item.genre}</p>
            );
        }

        return (
            <div className="page">
                <div className={"high3"}></div>
                <div className="homeTop">
                    <h3 className={"homeh3"}>Movies</h3>
                    <ul className="homeUl">
                        {tags}
                    </ul>
                </div>
                <div className={"slidercon"}>

                     <Slider currentMovie={this.props.currentMovie} getMovie={this.props.getMovie} movies={this.props.movies} />

                </div>

                <div className={"hometitle"}>
                    <NavLink to={"/Movie/" + tmp.id} className={"movieLink"}>
                        <h3 className="movie-Title">{tmp.title}</h3>
                    </NavLink>
                    <p className="movieTime">{tmp.time + ' min'}</p>
                    <Rating rating={tmp.rating}/>
                    <div className={"tagList"}>
                        {tagList}
                    </div>
                </div>

            </div>
        );
    }
}
/*
ReactDOM.render(<Carousel items={items} active={0}/>, document.getElementById('app'))
*/