import React, { Component } from "react";
import PropTypes from "prop-types";
import MaterialTitlePanel from "./material_title_panel";
import { Link } from 'react-router-dom';
import search from "./img/search.png";

const styles = {
    sidebar: {
        width: "17em"
    }
};

class UserNavigation extends Component {

    linkHandle = (e) =>{
        e.preventDefault();
        this.props.close();
        this.props.history.push(`/${e.target.id}`)
    }

    logout = (e) => {
        e.preventDefault();
        this.props.logout();
        this.props.history.push('/Home');
    }

    render() {

        let guest = <>
            <img className="img" src={'/img/' + this.props.tmp.cover} alt="user-image" />

            <div className="row">
                <div className="col-12">
                    <h3>{this.props.tmp.username}</h3>
                </div>
            </div>

            <div className="row">
                <div className="col-6">
                    <p>50 Following</p>
                </div>
            </div>
            <hr />
            <div className="row">
                <div className="col-12">
                    <div className="menu">
                        <Link id="Home" onClick={this.linkHandle}>
                            <img src="/img/search.png" alt="menu-icon"  />Home
                        </Link>
                        <hr />
                        <Link id="Login" onClick={this.linkHandle}>
                            <img src="/img/search.png" alt="menu-icon"  />Login
                        </Link>
                    </div>
                </div>
            </div>
            </>
        ;

        let user = <>
            <img className="img" src={(this.props.tmp.cover === 'Sheep.png' ? '/img/' : 'https://image.tmdb.org/t/p/w300_and_h450_bestv2') + this.props.tmp.cover} alt="user-image" />

            <div className="row">
                <div className="col-12">
                    <h3>{this.props.tmp.username}</h3>
                </div>
            </div>
            <hr />
            <div className="row">
            <div className="col-12">
                <div className="menu">
            <Link id="Home" onClick={this.linkHandle}>
                <img src="/img/search.png" alt="menu-icon"  />Home
            </Link>
            <Link id="Following" onClick={this.linkHandle}>
                <img src="/img/search.png" alt="menu-icon"  />Following ({this.props.following.length})
            </Link>
            <Link id="Settings" onClick={this.linkHandle}>
                <img src="/img/search.png" alt="menu-icon"  />Settings
            </Link>
            <hr />
            <Link id="Login" onClick={this.logout}>
                <img src="/img/search.png" alt="menu-icon"  />Logout
            </Link>
                </div>
            </div>
            </div>
        </>;

        let showing = this.props.tmp.username === "Guest" ? guest: user;

    return (
        <MaterialTitlePanel title="Menu" style={styles.sidebar}>
            <div className="user-nav">
                {showing}
            </div>
        </MaterialTitlePanel>
    );
    }
}


export default UserNavigation;