import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';

import Home from './Home'
import Movie from './Movie'
import Search from './Search'
import Actor from './Actor'
import Settings from './Settings'
import UserNav from './UserNavigation';
import Sidebar from "react-sidebar";
import TopNavigation from './TopNavigation'
import Messages from './Messages'
import Login from './Login'
import SettingsNotification from "./SettingsNotification";
import SettingsPassword from "./SettingsPassword";
import Dexie from 'dexie';
import AuthService from './AuthService';
import SubscribeService from './SubscribeService';
import {CSSTransition, TransitionGroup} from "react-transition-group";
import Following from "./Following";
var db = new Dexie("moviedb");
db.version(1).stores({
    movies: "&_id, &id, title, time, rating, director, overview, trailer, cover, backCover, tags, cast, popularity",
    tags: "&_id, &genre_id, genre"
});
db.version(2).stores({
    movies: "&_id, &id, title, time, rating, director, overview, trailer, cover, backCover, tags, cast, popularity",
    tags: "&_id, &genre_id, genre",
    actors: "&_id, backCover, cover, followers, following, id, movies, name, overview"
});
db.version(3).stores({
    movies: "&_id, &id, title, time, rating, director, overview, trailer, cover, backCover, tags, cast, popularity",
    tags: "&_id, &genre_id, genre",
    actors: "&_id, backCover, cover, followers, following, id, movies, name, overview",
    sync: "++id, type, data, token"
});

const mql = window.matchMedia(`(min-width: 800px)`);

class App extends Component {
    API_URL = '/api';

  constructor(props) {
    super(props);
    this.Auth = new AuthService(`${this.API_URL}/user/login`);
    this.Subscribe = new SubscribeService(this.Auth);
    this.state = {
        docked: mql.matches,
        open: false,
        movies: [],
        tags: [],
        actors: [],
        currentMovie: "",
        sliderMovie: "",
        currentActor: "",
        searchlistactor: [],
        searchlistmovies: [],
        user: {
            username: "Guest",
            cover: "Sheep.png"
        },
        following: []
    };

    this.getMovies();
    this.getTags();
    if(this.Auth.loggedIn())this.getFollowing();
  }

    getUser = (json) => {
        return fetch('/api/user/login', {
            method: 'POST',
            body: JSON.stringify(json),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(res => res.json())
        .then(user => {
            if(user.msg === undefined){
                this.setState({ user: user })
            }
            return Promise.resolve(user);
        })
    }

    getFollowing = () => {
        this.Auth.fetch(`${this.API_URL}/push/`,{
            method: "GET"
        }).then((res) => {
            this.setState({following: res})
        })
    }

    createUser = (json) => {
        return fetch('/api/user', {
            method: 'POST',
            body: JSON.stringify(json),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }).then(res => res.json())
            .then(user => {
                if(user.msg === undefined){
                    this.setState({ user: user })
                }
                return Promise.resolve(user);
            })
    }

    getMovies = () => {
        fetch('/api/movie')
            .then(res => res.json())
            .then(movies => {
                this.setState({ movies: movies },() => this.database())
            })
    }

    getMoviesByTag = (id) => {
        fetch('/api/movie/tag/' + id)
            .then(res => res.json())
            .then(movies => {
                this.setState({ movies: movies },() => this.database())
            })
    }

    getTags = () => {
        fetch('/api/movie/tags')
            .then(res => res.json())
            .then(tags => {
                this.setState({ tags: tags }, () => this.databasetags())
            })
    }

    getMovie = (id) => {
        fetch('/api/movie/' + id)
            .then(res => res.json())
            .then(movie => {
                this.setState({ currentMovie: movie })
            })
    }

    getSliderMovie = (id) => {
        fetch('/api/movie/' + id)
            .then(res => res.json())
            .then(movie => {
                this.setState({ sliderMovie: movie })
            })
    }

    search = (search) => {
        fetch('/api/movie/search/' + search)
            .then(res => res.json())
            .then(movie => {
                this.setState({ searchlistmovies: movie})
            })

        fetch('/api/actor/search/' + search)
            .then(res => res.json())
            .then(actor => {
                this.setState({ searchlistactor: actor}, () => this.databaseActor())
            })
    }

    getActor = (id) => {
        fetch('/api/actor/' + id)
            .then(res => res.json())
            .then(actor => {
                if (actor.name)
                {
                    this.isUserSubbed(actor.name).then((response) => {
                            if (response.msg === "User is Subscribed")
                            {
                                actor.isSubbed = true;
                            }
                            else {
                                actor.isSubbed = false;
                            }
                        this.setState({ currentActor: actor })
                        }
                    ).catch(() => {
                            console.log("test")
                            this.setState({currentActor: actor})
                        }
                    )

                }
            })
    }

    database = () => {
        db.transaction('rw', db.movies, async() => {
        this.state.movies.forEach(movie => {
            db.movies.put(movie);
        })
        })
    }

    databasetags = () => {
        db.transaction('rw', db.tags, async() => {
            this.state.tags.forEach(tag => {
                db.tags.put(tag);
            })
        })
    }

    databaseActor = () => {
        db.transaction('rw', db.actors, async() => {
            this.state.searchlistactor.forEach(actor => {
                db.actors.put(actor);
            })
        })
    }


    componentWillMount() {
      this.changeLoginStatus();
        mql.addListener(this.mediaQueryChanged);
        navigator.serviceWorker.ready.then(function(registration) {
            registration.sync
                .register("message-queue-sync")
                .then(function() { return registration.sync.getTags(); })
                .then(function(tags) {
                    console.log(tags);
                });
        });
    }

    componentWillUnmount() {
        mql.removeListener(this.mediaQueryChanged);
    }

    onSetOpen = (open) => {
        this.setState({ open });
    };

    mediaQueryChanged = () => {
        this.setState({
            docked: mql.matches,
            open: false
        });
    }

    toggleOpen = (ev) => {
        this.setState({ open: !this.state.open });

        if (ev) {
            ev.preventDefault();
        }
    }

    selectMovie = (id) => {
      let movie = this.state.movies.find(e => e.id === Number(id));
      this.setState({currentMovie: movie});
    }

    selectActor = (id) => {
        let actor = this.state.actors.find(e => e.id === Number(id));
        this.setState({currentActor: actor});
    }

    changeLoginStatus = () => {
        let userinfo = this.Auth.getUserInfo();
        this.setState({user: userinfo ? userinfo:this.state.user})
    }

    logout = () => {
        this.Auth.logout();
        this.setState({user: {
                username: "Guest",
                cover: "Sheep.png"
            }})
    }

    changePassword = (json) => {
        return this.Auth.fetch(`${this.API_URL}/user/`,{
            method: "PUT",
            body: JSON.stringify(json)
        }).then((res) => {
            return Promise.resolve(res)
        })
    }

    isUserSubbed = (actor) => {
        return this.Auth.fetch(`${this.API_URL}/push/` + actor,{
            method: "POST",
        }).then((res) => {
            return Promise.resolve(res)
        })
    }

    setSubbed = (bool) => {
        let actor = this.state.currentActor;
        actor.isSubbed = bool;
        this.setState({currentActor: actor});
    }


  render() {
      const sidebar = <Route path="/" render={(props) => <UserNav {...props} tmp={this.state.user} following={this.state.following} close={this.toggleOpen} logout={this.logout} />} />
      const sidebarProps = {
          sidebar,
          docked: this.state.docked,
          open: this.state.open,
          onSetOpen: this.onSetOpen,
          user: this.state.user
      };

    return (
        <Router>
              <Sidebar {...sidebarProps}>
                  <Route path="/" render={(props) => <TopNavigation {...props} menu={this.toggleOpen} IsHome={true}/>} />
                  <Route render={({location}) => (
                      <TransitionGroup>
                        <CSSTransition key={location.key} timeout={700} classname={"fade"}>
                              <Switch location={location}>
                                <Route exact path="/Home" render={(props) => <Home {...props} menu={this.toggleOpen} tags={this.state.tags} movies={this.state.movies} currentMovie={this.state.sliderMovie} getMovie={this.getSliderMovie} getMoviesByTag={this.getMoviesByTag} />} />
                                <Route exact path="/Following" render={(props) => <Following {...props} following={this.state.following} getFollowing={this.getFollowing} unsubscribeToPush ={this.Subscribe.unsubscribeToPush} />} />
                                <Route exact path="/Movie/:id" render={(props) => <Movie {...props} movie={this.state.currentMovie} setActor={this.selectActor} id={props.match.params.id} setMovie={this.selectMovie} getMovie={this.getMovie} />} />
                                <Route exact path="/Search" render={(props) => <Search {...props} movies={this.state.movies} selectMovie={this.selectMovie} searchlistmovies={this.state.searchlistmovies} searchlistactor={this.state.searchlistactor} search={this.search} /> } />
                                <Route exact path="/Actor/:id" render={(props) => <Actor {...props} actor={this.state.currentActor} setActor={this.getActor} id={props.match.params.id} subscribeToPush={this.Subscribe.subscribeToPush} unsubscribeToPush ={this.Subscribe.unsubscribeToPush} auth={this.Auth} isUserSubbed={this.isUserSubbed} setSubbed={this.setSubbed} />} />
                                <Route exact path="/Settings" render={(props) => <Settings {...props} auth={this.Auth} />} />
                                <Route exact path="/Messages" render={(props) => <Messages {...props} auth={this.Auth} />} />
                                <Route exact path="/Login" render={(props) => <Login {...props} auth={this.Auth} getUser={this.getUser} createUser={this.createUser} changeLoginStatus={this.changeLoginStatus} />} />
                                <Route exact path="/SettingsNotification" render={(props) => <SettingsNotification {...props} auth={this.Auth} subscribeToPush={this.Subscribe.subscribeToPush} unsubscribeToPush ={this.Subscribe.unsubscribeToPush} />} />
                                <Route exact path="/SettingsPassword" render={(props) => <SettingsPassword changePassword={this.changePassword} auth={this.Auth} {...props} />} />
                                <Route path="*" render={(props) => <Redirect to="/Home" /> }/>
                              </Switch>
                        </CSSTransition>
                    </TransitionGroup>
                    )} />
              </Sidebar>
        </Router>
    );
  }
}

export default App;
