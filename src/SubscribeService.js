/**
 * Service class for subscribing users against an API
 */
export default class SubscribeService {
    constructor(auth) {
        this.AuthSub = auth;
    }


    subscribeToPush = (type, id) => {
        this.Subscribe(type, id);
    };

    unsubscribeToPush = (type) => {
        let self = this;
        navigator.serviceWorker.ready.then(function(reg) {
            reg.pushManager.getSubscription().then(function(subscription) {
                self.unsubDB(type);
                subscription.unsubscribe().then(function(successful) {
                    console.log("Push Notfications unsubbed");
                }).catch(function(e) {
                    // Unsubscription failed
                });
            });
        });
    };

 unsubDB = (type) =>{
     let self = this;
    self.AuthSub.fetch(`/api/push/unsubscribe/` + type, {
        method: 'DELETE'
    }).catch(function(e) {
        console.log(e);
    }).then(res => {
        console.log("Unsubbed from DB");
    });
};

 Subscribe = (type, id) =>{
    if('serviceWorker' in navigator && 'PushManager' in window) {
        let self = this;
        if (Notification.permission === 'granted') {
            const vapidPublicKey = 'BG4OjQ4nTmlk1nmaOYr25632xUhHRIydMTtcGwcictoF_lmbMysMRujFFMKdYG5u5IT3nidxKi6HgarJ2c0yBFc';
            navigator.serviceWorker.ready.then(
                function (serviceWorkerRegistration) {
                    // Register to push events here
                    const applicationServerKey = self.urlBase64ToUint8Array(vapidPublicKey);
                    const options = {
                        userVisibleOnly: true,
                        applicationServerKey: applicationServerKey,
                    };
                    serviceWorkerRegistration.pushManager.subscribe(options).then(
                        function (pushSubscription) {

                            let subJson = pushSubscription.toJSON();
                            subJson.subtype = type;
                            subJson.actorId = id;
                            console.log(subJson);

                            self.AuthSub.fetch('/api/push/subscribe', {
                                method: 'POST',
                                body: JSON.stringify(subJson),
                            }).catch(error => console.error(error));
                        }, function (error) {
                            console.log(error);
                        }
                    );
                });

        }
        else if(Notification.permission === 'denied')
        {
            console.log("notifcations denied");
        }
        else {
            Notification.requestPermission(function (status) {
                console.log('Notification permission status:', status);
                self.Subscribe(type);
            });
        }
    }
};

urlBase64ToUint8Array = (base64String) => {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

}
