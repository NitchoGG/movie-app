import React, { Component } from 'react';
import Rating from "./Rating";
import { Link } from 'react-router-dom';
import { Offline, Online } from "react-detect-offline";

export default class Movie extends Component {

    backdropStyle = {

    }

    constructor(props) {
        super(props);
        this.props.getMovie(this.props.id);
        this.state = {
            showAll: false
        }
    }

    viewAllOnClick = () =>{
        this.setState({showAll: !this.state.showAll})
    }

    render() {
        let cast = [];
        let tmpmovie = [];
        if (this.props.movie) {

            if (this.props.movie.length > 0)
            {
                tmpmovie = this.props.movie[0]
            }
            else
            {
                tmpmovie = this.props.movie
            }


            let count = this.state.showAll? 1000: 5;

            let style = {backgroundColor: 'black'};

            for (let i = 0; i < count && i < tmpmovie.cast.length; i++)
            {
                cast.push(<div key={i} className="row mt-2">
                    <div className="col-2">
                        <div className="actor-image rounded-circle">
                            <Link to={'/Actor/' + tmpmovie.cast[i].id}>
                                <img className="center-block img-responsive full-width" style={style} src={tmpmovie.cast[i].image ? "https://image.tmdb.org/t/p/w138_and_h175_face" + tmpmovie.cast[i].image : '/img/Sheep.png'} />
                            </Link>
                        </div>
                    </div>
                    <div className="col-5 align-self-center">
                        <Link to={'/Actor/' + tmpmovie.cast[i].id} className={"castText"}>
                            {tmpmovie.cast[i].name}
                        </Link>
                    </div>
                    <div className="col-5 align-self-center castText">{tmpmovie.cast[i].role}</div>
                </div>)
            }

            this.backdropStyle = {
                backgroundImage: 'url(https://image.tmdb.org/t/p/w500_and_h282_face' + tmpmovie.backCover + ')',
                backgroundRepeat: "no-repeat",
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                height: '18em'
            }
        }

    return (
      <div className="container-fluid mb-3 movie page">
            <div className="row align-items-end" style={this.backdropStyle}>
                <div className="overlay"></div>
                <div className="col-12">
                    <div className="row">
                        <div className="col-5">

                        </div>
                        <div className="col-6 text-white mb-2">
                            <h2 className="movie-title font-weight-bold">{tmpmovie.title}</h2>
                            <Rating rating={tmpmovie.rating}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-12">
                    <div className="row">
                        <div className="col-5">
                            <div className="poster-image-container">
                                <img alt="movie cover" src={'https://image.tmdb.org/t/p/w185_and_h278_bestv2' + tmpmovie.cover} />
                            </div>
                        </div>
                        <div className="col-7 director">
                            <h3>Director</h3>
                            <span>{tmpmovie.director}</span>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-12 overview">
                            {tmpmovie.overview}
                                <div className="video-container mt-4">
                                    <iframe title="youtube trailer" width="100%"  src={"https://www.youtube.com/embed/" + tmpmovie.trailer + "?autoplay=0"} frameBorder="0" allowFullScreen />
                                </div>
                        </div>
                    </div>
                </div>
            </div>
          <hr className="mt-4" />
            <div className="row mt-3">
                <div className="col-12">
                    <div className="row">
                        <div className="col-8">
                            <h3 className={"movieCast"}>Cast</h3>
                        </div>
                        <div className="col-4 text-right">
                            <a onClick={this.viewAllOnClick}>{this.state.showAll ? "Hide" : "View All"}</a>
                        </div>
                    </div>
                    <div className="row cast mt-2">
                        <div className="col-12">
                            {cast}
                        </div>
                    </div>
                </div>
            </div>
      </div>
    );
  }
}
