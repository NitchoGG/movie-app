import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Settings extends Component {
    componentWillMount() {
        if (!this.props.auth.loggedIn()) this.props.history.push('/Home');
    }

    render() {
        return (

                    <div className="container-fluid page">
                        <div className={"high3"}></div>
                        <img className="user-image" src="./img/search.png" alt="user-image" />

                        <div className="row">
                            <div className="col-11 settings">
                                <Link to={"/SettingsNotification"}>Notifications <p>&#62;</p></Link>
                                <Link to={"/SettingsPassword"}>Change Password <p>&#62;</p></Link>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-11 settings">
                                <a href="#">Logout</a>
                            </div>
                        </div>
                    </div>

        );
    }
}