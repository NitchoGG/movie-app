import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            signUp : false
        };
    }

    spanClick = () =>{
        this.setState({
            signUp:true
        });
    };

    handleLogin =(e) =>{
        e.preventDefault();
        this.props.auth.login(this.username.value,this.password.value)
            .then(response => {
                if (response.msg === 'User authenticated successfully')
                {
                    this.props.changeLoginStatus();
                    this.props.history.push(`/Home`)
                }
                else if(response.msg === 'Username or password missing!')
                {
                    this.lbl.innerHTML = "Username or password missing!";
                }
                else
                {
                    this.label.innerHTML = "An error happened";
                }
            })
    }

    handleRegister =(e) =>{
        e.preventDefault();
        this.props.createUser({ username: this.username.value, password: this.password.value, email: this.email.value}).then(err => {
            if(err.msg) {
                this.lbl.innerHTML = err.msg;
            }else{
                //this.props.history.push(`/Home`)
            }
        });
    }

    render() {
        let content =
            <div className="loginBackground">
                <div className={"loginBlur"}>

                </div>
                <div className={"loginContainer"}>
                    <form className={"loginForm"}>
                        <label className="err" ref={lbl => this.lbl = lbl} />
                        <input ref={username => this.username = username} placeholder={"Username"}/>
                        <input type="password" ref={password => this.password = password} placeholder={"Password"}/>
                        <button type={"submit"} onClick={this.handleLogin}>Sign in</button>
                    </form>
                    <p>Don't have an account? <span onClick={this.spanClick} className={"signUp"}>Sign up</span></p>
                </div>
            </div>;

        if(this.state.signUp)
        {
            content =
            <div className="loginBackground">
                <div className={"loginBlur"}>

                </div>
                <div className={"loginContainer"}>
                    <form className={"loginForm"}>
                        <label className="err" ref={lbl => this.lbl = lbl} />
                        <input ref={username => this.username = username} placeholder={"Username"}/>
                        <input type="password" ref={password => this.password = password} placeholder={"Password"}/>
                        <input ref={email => this.email = email} placeholder={"Email"}/>
                        <button type={"submit"} onClick={this.handleRegister}>Create Account</button>
                    </form>
                </div>
            </div>;
        }

        return content;
    }
}