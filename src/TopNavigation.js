import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class TopNavigation extends Component {

    goBack = (event) =>{
        this.props.history.goBack();
    }

  render() {
    let content =
      <div className="topNavigation">
          <a onClick={this.goBack}>
              <img className="imgLeft" src="/img/back-button.png" alt="menu-icon" />
          </a>
          <img className="imgLeft" onClick={this.props.menu} src="/img/burger-menu.png" alt="menu-icon"/>

          <Link to={"/Search"}>
              <img className="imgRight" src="/img/search.png" alt="menu-icon"  />
          </Link>
      </div>;

      return content;
  }
}
