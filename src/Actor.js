import React, { Component } from 'react';
import {Link} from "react-router-dom";

export default class Actor extends Component {

    backdropStyle = {

    };

    constructor(props) {
        super(props);
        this.props.setActor(this.props.id)
    }

    componentDidMount() {

    }

    subScribeToPush = () =>{
        if(this.props.actor)
        {
            this.props.subscribeToPush(this.props.actor.name, this.props.id);
            this.props.setSubbed(true);
        }
    };

    unsubscribeToPush = () =>{
        this.props.unsubscribeToPush(this.props.actor.name);
        this.props.setSubbed(false);
    };


    render() {
        let movies = [];
        let tmpactor = [];
        let subButton = "";


        if (this.props.actor && this.props.actor.movies) {
            tmpactor = this.props.actor;


            this.props.actor.movies.forEach((movie, index) => {
                if(movie.id)
                {
                    movies.push(<Link key={index} to={"/Movie/" + movie.id} className="movielink">
                        <img src={'https://image.tmdb.org/t/p/w150_and_h225_bestv2' + movie.image} />
                    </Link>)
                }
                else
                {
                    movies.push(<a key={index} className="movielink">
                        <img src={'https://image.tmdb.org/t/p/w150_and_h225_bestv2' + movie.image} />
                    </a>)
                }
                }
            );

            if(this.props.auth.loggedIn())
            {
                if(this.props.actor.isSubbed){
                    subButton = <button className={"sub"} onClick={this.unsubscribeToPush}> </button>;
                }
                else{
                    subButton = <button className={"nosub"} onClick={this.subScribeToPush}> </button>;
                }
            }

            let  backgroundimage = 'url(' + (tmpactor.backCover? 'https://image.tmdb.org/t/p/w500_and_h282_face' + tmpactor.backCover : '/img/loginBackground.jpg') + ')';

            this.backdropStyle = {
                backgroundImage: backgroundimage,
                backgroundRepeat: "no-repeat",
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                height: '18em'
            }
        }

    return (
      <div className="container-fluid mb-3 page">
            <div className="row text-white align-items-center" style={this.backdropStyle}>
                <div className="col-12">
                    <div className="row">
                        <div className="col-12">
                            <h2 className={"actorName"}>{tmpactor.name}</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <span className={"actorMovies"}>Movies 56</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <div className="row followtext">
                        <div className="col-6">
                            <div className="actor-image-container">
                                <img className="" src={'https://image.tmdb.org/t/p/w300_and_h450_bestv2' + tmpactor.cover} alt='actor' />
                            </div>
                        </div>
                        <div className="col-2 followers">
                            <h3>Followers</h3>
                            <span>{tmpactor.followers}</span>
                        </div>
                        <div className="col-1">
                        </div>
                        <div className="col-3">
                            {subButton}
                        </div>
                    </div>
                    <div className="row mt-3 bio">
                        <div className="col-12 overview">
                            {tmpactor.biography}
                        </div>
                    </div>
                </div>
            </div>
          <hr className="mt-4" />
            <div className="row mt-3">
                <div className="col-12">
                    <div className="row">
                        <div className="col-8">
                            <h3 className={"movieCast"}>Movies</h3>
                        </div>
                        <div className="col-4 text-right">
                            <a href="#" className={"viewAll"}>View All</a>
                        </div>
                    </div>
                    <div className="row cast mt-2">
                        <div className="col-12">
                            <div className="movie-container">
                                {movies}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    );
  }
}
