import React, {Component} from 'react';
import { TransitionGroup } from 'react-transition-group';
import { BrowserRouter as Router, Switch, Route, Link, NavLink } from 'react-router-dom';

export default class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            active: 0,
            direction: ''
        }

        this.rightClick = this.moveRight.bind(this)
        this.leftClick = this.moveLeft.bind(this)
    }

    generateItems() {
        let items = [];
        let level;
        if(this.props.movies){
            for (let i = this.state.active - 3; i < this.state.active + 4; i++) {
                let index = i;
                if (i < 0) {
                    index = this.props.movies.length + i
                } else if (i >= this.props.movies.length) {
                    index = i % this.props.movies.length
                }
                level = this.state.active - i;
                if (level === 0 && this.props.currentMovie.id != this.props.movies[index].id) this.props.getMovie(this.props.movies[index].id)
                items.push(<Item getMovie={this.props.getMovie} key={index} movie={this.props.movies[index]} level={level} />)
            }
        }
        return items
    }

    moveLeft() {
        let newActive = this.state.active
        newActive--
        this.setState({
            active: newActive < 0 ? this.props.movies.length - 1 : newActive,
            direction: 'left'
        })
    }

    moveRight() {
        let newActive = this.state.active
        this.setState({
            active: (newActive + 1) % this.props.movies.length,
            direction: 'right'
        })
    }

    render() {
        let myslide;

        if(this.props.movies.length > 0){
            myslide = <>
                <div className="arrow arrow-left" onClick={this.leftClick}>
                    <i className="fi-arrow-left" />
                </div>
                <TransitionGroup
                    transitionname={this.state.direction}>
                    {this.generateItems()}
                </TransitionGroup>
                <div className="arrow arrow-right" onClick={this.rightClick}>
                    <i className="fi-arrow-right" />
                </div>
            </>
        }


        return (
            <div id="carousel" className="noselect">
                {myslide}
            </div>
        );
    }
}

class Item extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            level: this.props.level
        }
    }

    render() {

        const className = 'item level' + this.props.level
        return(
            <div className={className}>
                <NavLink to={"/Movie/" + this.props.movie.id} className="movielink">
                    <img alt="poster" src={'https://image.tmdb.org/t/p/w300_and_h450_bestv2' + this.props.movie.cover} />
                </NavLink>
            </div>
        )
    }
}