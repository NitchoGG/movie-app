import React, { Component } from 'react';


export default class SettingsNotification extends Component {

    componentWillMount() {
        if (!this.props.auth.loggedIn()) this.props.history.push('/Home');
    }

    subFunction = () => {
        let type = "Actor";
        this.props.subscribeToPush(type);
    };

    render() {

        return (
            <div className="container-fluid page">
                <div className={"high3"}></div>
                <div className="row">
                    <div className="col-11 settings-notification">
                        <div className="div">
                            <p>New Follower</p>
                            <label className="switch ">
                                <input type="checkbox" className="success"/>
                                <span className="slider round" onClick={this.subFunction}/>
                            </label>
                        </div>
                        <div className="div">
                            <p>New Movie</p>
                            <label className="switch ">
                                <input type="checkbox" className="success"/>
                                <span className="slider round" onClick={this.props.unsubscribeToPush}/>
                            </label>
                        </div>
                        <div className="div">
                            <p>New Message</p>
                            <label className="switch ">
                                <input type="checkbox" className="success"/>
                                <span className="slider round"/>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
