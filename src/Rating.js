import React, { Component } from 'react';

export default class Rating extends Component {

    handleClick = () => {

    }

  render() {
        let stars = [];
        
        for (let i = 0; i < (this.props.rating/2) && i < 5; i++)
        {
            stars.push(<svg key={'redstar' + i} width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.94737 0L8.50715 3.73373H13.5547L9.47115 6.04131L11.0309 9.77504L6.94737 7.46747L2.86381 9.77504L4.42359 6.04131L0.340028 3.73373H5.38759L6.94737 0Z" fill="#FF3B30"/>
            </svg>)
        }

        for (let i = (this.props.rating/2); i < 4; i++)
        {
            stars.push(<svg key={'graystar' + i} width="14" height="10" viewBox="0 0 14 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M7.05262 0L8.61241 3.73373H13.66L9.5764 6.04131L11.1362 9.77504L7.05262 7.46747L2.96906 9.77504L4.52884 6.04131L0.445283 3.73373H5.49284L7.05262 0Z" fill="#C4C4C4"/>
            </svg>)
        }

      return (
      <span>
          {stars} {Math.round((this.props.rating/2)*10)/10}/5
      </span>
    );
  }
}
