import React, { Component } from 'react';
import search from './img/search.png' // relative path to image
import {Link, NavLink} from 'react-router-dom';

export default class Search extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            searchText : ""
        }
    }

    onChange = event => {
        if (event.target.value.length > 0) this.props.search(event.target.value);
        this.setState({
            searchText : event.target.value
        });
    };

    clearSearch = () =>{
        this.setState({
            searchText : ""
        });
    };

    home = () =>{
        this.props.history.push("/Home")
    };



    render() {
        let items = [];
        let searchItems = [];
            if (this.props.searchlistmovies)
            {
                searchItems = [...this.props.searchlistmovies]
            }

            if (this.props.searchlistactor)
            {
                searchItems = [...searchItems, ...this.props.searchlistactor]
            }
            searchItems = searchItems.sort((a,b) => {
                if (a.name && b.name)
                {
                    if (a.name < b.name)
                    {
                        return -1;
                    }
                    if (a.name > b.name)
                    {
                        return 1;
                    }
                    return 0;
                }
                else if (a.name && b.title)
                {
                    if (a.name < b.title)
                    {
                        return -1;
                    }
                    if (a.name > b.title)
                    {
                        return 1;
                    }
                    return 0;
                }
                else if(a.title && b.name)
                {
                    if (a.title < b.name)
                    {
                        return -1;
                    }
                    if (a.title > b.name)
                    {
                        return 1;
                    }
                    return 0;
                }
                else if(a.title && b.title)
                {
                    if (a.title < b.title)
                    {
                        return -1;
                    }
                    if (a.title > b.title)
                    {
                        return 1;
                    }
                    return 0;
                }
            })

        searchItems.forEach(item => {
            items.push(<div className="row mt-2">
                <div className="col-2 cast">
                    <div className="actor-image rounded-circle">
                        <NavLink to={(item.name?'/Actor/':'/Movie/') + item.id}>
                            <img id={item.id} className="center-block img-responsive full-width" src={"https://image.tmdb.org/t/p/w185_and_h278_bestv2" + item.cover} />
                        </NavLink>
                    </div>
                </div>
                <div className="col-10 align-self-center searchItemText">{item.name? item.name : item.title}</div>
            </div>)
        })

        return (
            <div className="container-fluid page">
                <div className={"high3"}></div>
                <div className={"row searchContainer"}>
                    <div className={"col-12 search"}>
                        <div className={"row"}>
                            <div className={"col-10"}>
                                <img className="imgSearch" src={search} alt={search}/>
                                <input onChange={this.onChange} className="inputSearch" placeholder={"Search"}/>
                                <button onClick={this.clearSearch} hidden={true} className={"inputClear"}>X</button>
                            </div>
                            <div>
                                <button className={"btnCancel"} onClick={this.home}>Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={"searchContent"}>
                    <h4>Recent Searches</h4>
                    {items}
                </div>
            </div>
        );
    }
}
