import React, { Component } from 'react';

export default class SettingsPassword extends Component {

    componentWillMount() {
        if (!this.props.auth.loggedIn()) this.props.history.push('/Home');
    }

    render() {
        return (
            <div className="container-fluid page">
                <div className={"high3"}></div>
                <div className="row">
                    <div className="col-11 settings-password">
                        <label className="err" ref={lbl => this.lbl = lbl} />
                        <input ref={oldPassword => this.oldPassword = oldPassword} type="text" className="text-cos" placeholder="Old Password" />
                        <input ref={newPassword1 => this.newPassword1 = newPassword1} type="text" className="text-cos" placeholder="New Password" />
                        <input ref={newPassword2 => this.newPassword2 = newPassword2} type="text" className="text-cos" placeholder="Repeat Password" />
                        <button onClick={this.onChangePasswordClick} className="btn-cos">Change Password</button>
                    </div>
                </div>

            </div>
        );
    }

    onChangePasswordClick = (e) => {
        e.preventDefault();
        if (this.newPassword1.value !== this.newPassword2.value){
            this.lbl.innerHTML = "Passwords dont match!!";
        }
        else
        {
            this.props.changePassword({password: this.oldPassword.value, newpassword: this.newPassword1.value}).then(response => {
                if (response.msg) this.lbl.innerHTML = response.msg;
            })
        }
    }
}