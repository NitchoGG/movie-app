import React, { Component } from 'react';

export default class Following extends Component {


    constructor(props) {
        super(props);
        this.props.getFollowing();
    }

    unsubScribe = (e) => {
        e.preventDefault();
        this.props.unsubscribeToPush(e.target.id);
        this.props.getFollowing();
    }


    render() {

        let following = [];

        if (this.props.following)
        {
            this.props.following.forEach(follow => {
                following.push
                    (<div className="row mt-2">
                            <div className="col-2 cast">
                                <div className="actor-image rounded-circle">

                                    <img className="center-block img-responsive full-width" src={"https://image.tmdb.org/t/p/w185_and_h278_bestv2" + follow.actorImage} />

                                </div>
                            </div>
                            <div className="col-8 align-self-center searchItemText">{follow.actor}</div>
                            <div className="col-2"><button className={"nosub"} id={follow.subtype} onClick={this.unsubScribe}></button></div>
                        </div>
                )
            })
        }

        return (
            <div className="container-fluid page">
                <div className={"high3"}></div>
                <div className={"searchContent"}>
                    <h4>Following</h4>
                    {following}
                </div>
            </div>
        );
    }
}