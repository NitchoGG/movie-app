module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();

    //underskema til tags i movie skemaet
    let tagsSchema = mongoose.Schema({
        genreid: Number,
        genre: String
    });

    //underskema til tags i movie skemaet
    let castSchema = mongoose.Schema({
        id: Number,
        name: String,
        image: String,
        role: String
    });

    //skema til film
    let movieSchema = mongoose.Schema({
        id: Number,
        title: String,
        time: Number,
        rating: Number,
        director: String,
        overview: String,
        trailer: String,
        cover:String,
        backCover: String,
        tags: [tagsSchema],
        cast: [castSchema],
        popularity: Number
    });

    let movieModel = mongoose.model('movie', movieSchema);
    let tagModel = mongoose.model('tags', tagsSchema);

    //rute til at hente de 10 mest populære film
    router.get('/', (req,res) => {
        movieModel.find().sort({popularity: -1}).limit(10).exec((err, questions) => {
            if (err) return console.log(err);
            res.json(questions);
        })
    });

    //rute til at hente de 10 mest populære film udfra genre id
    router.get('/tag/:id', (req,res) => {
        let tag = Number(req.params.id);
        movieModel.find()
            .where('tags.genreid').equals(tag)
            .sort({'popularity': -1})
            .limit(10)
            .exec(function (err, movies) {
                if (err) console.log(err)
                res.send(movies);
            });

    });

    //rute til at hente alle tags
    router.get('/tags', (req,res) => {
        tagModel.find((err, tags) => {
            if (err) return console.log(err);
            res.json(tags);
        })
    });

    //rute til at hente en film udfra dens id
    router.get('/:id', (req,res) => {
        movieModel.findOne({id: Number(req.params.id)}, (err, movie) => {
            if (err) return console.log(err);
            res.json(movie);
        });
    });

    //rute til at søge efter film
    router.get('/search/:search', (req,res) => {
        movieModel.find({title: {$regex: new RegExp(req.params.search, 'i')}}).limit(10).exec((err, movie) => {
            if (err) return console.log(err);
            res.json(movie);
        });
    });

    return router;
}