module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();
    let webpush = require('web-push');

    let subscribeModel = mongoose.model('subscriber');
    let movieModel = mongoose.model('movie');
    let actorModel = mongoose.model('actor');

    const publicVapidKey = "BG4OjQ4nTmlk1nmaOYr25632xUhHRIydMTtcGwcictoF_lmbMysMRujFFMKdYG5u5IT3nidxKi6HgarJ2c0yBFc";
    const privateVapidKey = "Gg1p-BtKrPlpg4qoDvnarmotKonOWI3sgYJ71D7jwLs";

    webpush.setVapidDetails('mailto:nitchogg@gmail.com', publicVapidKey, privateVapidKey);


    let movies = [
        {
            "id" : 2,
            "title" : "Black Panther",
            "time" : 134,
            "rating" : 7.4,
            "popularity" : 60.322,
            "director" : "Ryan Coogler",
            "overview" : "King T'Challa returns home from America to the reclusive, technologically advanced African nation of Wakanda to serve as his country's new leader. However, T'Challa soon finds that he is challenged for the throne by factions within his own country as well as without. Using powers reserved to Wakandan kings, T'Challa assumes the Black Panther mantel to join with girlfriend Nakia, the queen-mother, his princess-kid sister, members of the Dora Milaje (the Wakandan 'special forces') and an American secret agent, to prevent Wakanda from being dragged into a world war.",
            "trailer" : "xjDjIWPwcPU",
            "cover" : "/uxzzxijgPIY7slzFvMotPv8wjKA.jpg",
            "backCover" : "/1Jj7Frjjbewb6Q6dl6YXhL3kuvL.jpg",
            "tags" : [
                {
                    "genreId" : 878,
                    "genre" : "Science Fiction"
                },
                {
                    "genreId" : 28,
                    "genre" : "Action"
                },
                {
                    "genreId" : 12,
                    "genre" : "Adventure"
                },
                {
                    "genreId" : 14,
                    "genre" : "Fantasy"
                }
            ],
            "cast" : [
                {
                    "id" : 6,
                    "name" : "Chadwick Boseman",
                    "image" : "/znyHJBN6r0cONV17R26U31R6VqR.jpg",
                    "role" : "T'Challa / Black Panther"
                },
                {
                    "id" : 7,
                    "name" : "Michael B. Jordan",
                    "image" : "/4vXkRCbrquoP5Lrb4px7tubDLMC.jpg",
                    "role" : "N'Jadaka / Erik 'Killmonger' Stevens"
                },
                {
                    "id" : 8,
                    "name" : "Lupita Nyong'o",
                    "image" : "/l8RXyGKcCqEagjEXOm8iUaQtKdW.jpg",
                    "role" : "Nakia"
                },
                {
                    "id" : 9,
                    "name" : "Danai Gurira",
                    "image" : "/xgo39kFf6rAynb1i9J1BeLfSXxg.jpg",
                    "role" : "Okoye"
                },
                {
                    "id" : 10,
                    "name" : "Martin Freeman",
                    "image" : "/EOujE6oH4TdqShKWfsiBaup4FS.jpg",
                    "role" : "Everett K. Ross"
                }
            ]
        }
    ]

    router.post('/subscribe', (req, res) => { // Store subscription on server
        let s = req.body;
        s.username = req.user.username;


        subscribeModel.countDocuments({endpoint:s.endpoint,subtype:s.subtype}, (err, count) => {
            if(count > 0)
            {
                res.json({
                    msg: 'User already subbed'
                });

            }
            else
            {
                actorModel.findOne({id: Number(req.body.actorId)}, (err, actor) => {
                    if (actor)
                    {
                        s.actorImage = actor.cover;
                        s.actor = actor.name;
                        let sub = new subscribeModel(s);
                        sub.save()
                    }
                })
                res.json({ msg: 'Subscription saved.' });
            }
        });
    });

    router.get('/', (req, res) => {
        subscribeModel.find({username : req.user.username}, (err, subscriptions) => {
            if (subscriptions)
            {
                res.json(subscriptions);
            }
            else
            {
                res.json({
                    msg: "an error occurred"
                })
            }
        })
    })

    router.post('/:actor', (req, res) => {
        subscribeModel.countDocuments({subtype:req.params.actor,username : req.user.username}, (err, count) => {
            if(count > 0)
            {
                res.json({
                    msg:"User is Subscribed"
                })
            }
            else{
                res.json({
                    msg:"User is not subbed"
                })
            }
        });
    });

    router.delete('/unsubscribe/:subtype', (req,res) => {
        let type = req.params.subtype;
        console.log(type);
        subscribeModel.deleteOne({subtype:type,username:req.user.username}, function(err){
            if(err) return console.log(err);

            res.json("Deleted subscription")
        });
    });

    router.post('/push_message/:id', (req, res) => {
        SendPushMessage(req.params.id, req.body.title, req.body.text)
        res.json({message: "Sending push messages initiated"});
    });

    setTimeout(testTimer, 60 * 1000)

    function testTimer() {
        checkformovie();
        setTimeout(testTimer, 60 * 1000)
    }

    function checkformovie() {
        movies.forEach(movie => {
            let temp = new movieModel(movie);
            temp.cast.forEach(actor => {
                SendPushMessage(actor.name, "test besked", "ny film tilføjet", movie.id)
            })
        })
    }

    function SendPushMessage(id, title, text, movieid) {
        subscribeModel.find({subtype:id}, (err, subscriptions) => {
            subscriptions.forEach((sub) => {
                const payload = JSON.stringify({
                    msg: text,
                    title: title,
                    id: movieid
                });
                webpush.sendNotification(sub, payload).catch(error => {
                    console.error(error.stack);
                });
            });
        });
    }

    return router;
};