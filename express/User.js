module.exports = (mongoose) => {
    const express = require('express');
    const router = express.Router();
    const bcrypt = require('bcrypt');
    const jwt = require('jsonwebtoken');

    //subsribe skema
    let SubscriberSchema = mongoose.Schema({
        endpoint: String,
        keys: mongoose.Schema.Types.Mixed,
        createDate: {
            type: Date,
            default: Date.now
        },
        subtype : String,
        username : String,
        actor: String,
        actorImage : String,
        actorId: Number,
    });

    //bruger skema
    let userSchema = mongoose.Schema({
        username: String,
        password: String,
        email: String,
        cover: String
    });

    let userModel = mongoose.model('user', userSchema);
    let subscribeModel = mongoose.model('subscriber', SubscriberSchema);

    //rute til at hente en bruger
    router.get('/:id', (req,res) => {
        userModel.findById(req.params.id, (err, user) => {
            if (err) return console.log(err);
            res.json(user);
        });
    });

    //rute til login
    router.post('/login', (req,res) => {
        const username = req.body.username;
        const password = req.body.password;

        if (!username || !password) {
            let msg = "Username or password missing!";
            console.error(msg);
            res.json({msg: msg});
            return;
        }

        //kigger om brugeren eksisterer
        userModel.findOne({ username: username}, (err, user) => {
            if (user) {
                //sammenligner password i db
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {
                        subscribeModel.countDocuments({username: username}, (err, count) => {
                            const payload = {
                                username: username,
                                email: user.email,
                                cover: user.cover ? user.cover : 'Sheep.png',
                                admin: false,
                                following: count
                            };
                            const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '365d' });

                            res.json({
                                msg: 'User authenticated successfully',
                                token: token
                            });
                        });




                    }
                    else res.json({msg: "Password mismatch!"})
                });
            } else {
                res.json({msg: "User not found!"});
            }
        })
    });

    router.post('/', (req, res) => {
        if (!req.body.password && !req.body.username && !req.body.email)
        {
            res.json({msg: "username, password and email missing"})
        }
        else if (!req.body.password && !req.body.email)
        {
            res.json({msg: "password and email missing"})
        }
        else if (!req.body.username && !req.body.email)
        {
            res.json({msg: "username and email missing"})
        }
        else if (!req.body.password && !req.body.username)
        {
            res.json({msg: "username and password missing"})
        }
        else if (!req.body.password)
        {
            res.json({msg: "password missing"})
        }
        else if(!req.body.username)
        {
            res.json({msg: "username missing"})
        }
        else if(!req.body.email)
        {
            res.json({msg: "email missing"})
        }
        else
        {
            let newUser = new userModel();
            userModel.find({username: req.body.username}, (err, result) => {
                if (result.length > 0)
                {
                    res.json({msg: "username allready taken"});
                }
                else
                {
                    bcrypt.hash(req.body.password, 10, function(err, hash) {
                        if (err)
                        {
                            res.json({msg: "Error " + err})
                        }
                        else
                        {
                            newUser.username = req.body.username;
                            newUser.password = hash;
                            newUser.email = req.body.email
                            newUser.save();
                            res.json({msg: "User created"});
                        }
                    });
                }

            })

        }});

    router.put('/', (req,res) => {
        let password = req.body.password;
        let newpassword = req.body.newpassword;

        if (password)
        {
            userModel.findOne({ username: req.user.username}, (err, user) => {
                if (user) {
                    bcrypt.compare(password, user.password, (err, result) => {
                        if (result) {
                            if (newpassword)
                            {
                                bcrypt.hash(newpassword, 10, function(err, hash) {
                                    if (err)
                                    {
                                        res.json({msg: "Error " + err})
                                    }
                                    else
                                    {
                                        user.password = hash;
                                        user.save();
                                        res.json({msg: "userinfo updated"});
                                    }
                                });
                            }
                            else
                            {
                                user.save();
                                res.json("userinfo updated");
                            }
                        }
                        else res.status(401).json({msg: "Password mismatch!"})
                    });
                } else {
                    res.status(404).json({msg: "User not found!"});
                }
            })
        }
        else
        {
            res.status(401).json({msg: "password missing"});
        }
    });

    return router;
}