module.exports = (mongoose) => {
    let express = require('express');
    let router = express.Router();


    //underskema til film i actors
    let movieSchema = mongoose.Schema({
        title: String,
        image: String
    });

    //actor skema
    let actorSchema = mongoose.Schema({
        name: String,
        followers: Number,
        following: Number,
        director: String,
        overview: String,
        cover:String,
        backCover: String,
        movies: [movieSchema],
    });

    let actorModel = mongoose.model('actor', actorSchema);

    //rute til at hente en specifik actor udfra deres id
    router.get('/:id', (req,res) => {
        actorModel.findOne({id: Number(req.params.id)}, (err, question) => {
            if (err) return console.log(err);
            res.json(question);
        });
    });

    //rute til at søge efter actors
    router.get('/search/:search', (req,res) => {
        actorModel.find({name: {$regex: new RegExp(req.params.search, 'i')}}).limit(10).exec((err, actor) => {
            if (err) return console.log(err);
            res.json(actor);
        });
    });

    return router;
}