const compression = require('compression')
const path = require('path');
const express = require('express');
const bodyparser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const webpush = require('web-push');
const port = (process.env.PORT || 3001);
const manifest = require('../build/asset-manifest.json');
const fs = require('fs');
const checkJwt = require('express-jwt');
const pathToRegexp = require('path-to-regexp');
const swPath = '../build/serviceworker.js';
const urlsCSV = Object.keys(manifest.files)
    .filter(k => (k.includes('precache-manifest')))
    .map(k => manifest.files[k]);

//kigger om precache-manifest filen er fundet
if (urlsCSV.length > 0)
{
    //læser precache-manifest filen fra build mappen
    fs.readFile("../build/" + urlsCSV[0], "utf8", (err, data) => {
        //regex til at filtrere alt undtagen filnavnet væk
        let regex = /(?:\"url\":([\s\S]*?)\n)/g;
        let strToMatch = data;
        let fileArray = [];
        //looper igennem alle matches i precache filen
        while ((match = regex.exec(strToMatch)) != null) {
            fileArray.push(match[1].replace(/"/g, "").trim())
        }

        //læser serviceworker filen fra build mappen og replacer %MANIFESTURLS% string med react filer der skal caches
        fs.readFile(swPath, "utf8", (err, data) => {
            if (err) { return console.log("Error trying to read SW file", err); }

            const result = data.replace("%MANIFESTURLS%", JSON.stringify(fileArray));

            fs.writeFile(swPath, result, "utf8", err => {
                if (err) { return console.log("Error trying to write SW file", err); }
            });
        });
    })
}

//kigger om JWT_SECRET variablen er sat
if (!process.env.JWT_SECRET) {
    console.error('You need to put a secret in the JWT_SECRET env variable!');
    process.exit(1);
}

//redirector fra http til https på heroku hvis NODE_ENV er sat til production
if(process.env.NODE_ENV === 'production') {
    app.use((req, res, next) => {
        if (req.header('x-forwarded-proto') !== 'https')
            res.redirect(`https://${req.header('host')}${req.url}`)
        else
            next()
    })
}

//tilføjer compression på filerne der serves af react serveren
app.use(compression())
app.use(bodyparser.urlencoded({
    extended: true
}));
app.use(bodyparser.json());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

    // intercepts OPTIONS method
    if ('OPTIONS' === req.method) {
        // respond with 200
        console.log("Allowing OPTIONS");
        res.sendStatus(200);
    } else {
        // move on
        next();
    }
});
app.use(morgan('combined'));

//server static filer fra react
app.use(express.static(path.join(__dirname, '../build')));

//database forbindelse til mongo
//mongoose.connect('mongodb://dbuser:niels2502@192.168.1.142/movie?authSource=admin', {useNewUrlParser: true});
mongoose.connect('mongodb://dbuser:niels2502@5.186.60.140/movie?authSource=admin', {useNewUrlParser: true});

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected to Mongodb")
});

//metode der bruges til ruter i api'en der ikke kræver en token
let openPath = function(req) {
    if (req.path === '/api/user/login')return true;
    else if(req.path === '/api/user' && req.method === "POST") return true;
    else if(pathToRegexp('/api/movie/tag/:id').test(req.path)) return true;
    else if(req.method === "GET")
    {
        if(req.path === '/api/movie') return true;
        else if(req.path === '/api/movie/tags') return true;
        else if(pathToRegexp('/api/movie/tag/:id').test(req.path)) return true;
        else if(pathToRegexp('/api/actor/:id').test(req.path)) return true;
        else if(pathToRegexp('/api/movie/:id').test(req.path)) return true;
        else if(pathToRegexp('/api/movie/search/:search').test(req.path)) return true;
        else if(pathToRegexp('/api/actor/search/:search').test(req.path)) return true;
        else if(req.path === '/') return true;
        else if(req.path === '/Home') return true;
        else if(req.path === '/index.html') return true;
        else if(req.path === '/Login') return true;
        else if(req.path === '/Settings') return true;
        else if(req.path === '/SettingsPassword') return true;
        else if(req.path === '/SettingsNotification') return true;
        else if(req.path === '/Messages') return true;
        else if(req.path === '/Search') return true;
        else if(req.path === '/Following') return true;
        else if(pathToRegexp('/Movie/:id').test(req.path)) return true;
        else if(pathToRegexp('/Actor/:id').test(req.path)) return true;
    }
    return false;
}

// valider brugen ved hjælp af authentication
app.use(
    checkJwt({ secret: process.env.JWT_SECRET }).unless(openPath)
);
app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({ error: err.message });
    }
});

//eksterne filer med flere api ruter i
let User = require('./User')(mongoose);
let Movie = require('./Movie')(mongoose);
let Actor = require('./Actor')(mongoose);
let Push = require('./Push')(mongoose);
app.use('/api/user', User);
app.use('/api/movie', Movie);
app.use('/api/actor', Actor);
app.use('/api/push', Push);


app.use((err, req, res, next) =>{
    console.error(err.stack);
    res.status(500).send({msg: 'Something Broke'})
});

/**** Reroute all unknown requests to the React index.html ****/
app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.listen(port, () => console.log(`Api running on port ${port}!`));