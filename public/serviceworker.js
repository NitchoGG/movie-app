importScripts("/dexie.js");
var db = new Dexie("moviedb");
db.version(1).stores({
    movies: "&_id, &id, title, time, rating, director, overview, trailer, cover, backCover, tags, cast, popularity",
    tags: "&_id, &genre_id, genre"
});
db.version(2).stores({
    movies: "&_id, &id, title, time, rating, director, overview, trailer, cover, backCover, tags, cast, popularity",
    tags: "&_id, &genre_id, genre",
    actors: "&_id, backCover, cover, followers, following, id, movies, name, overview"
});
db.version(3).stores({
    movies: "&_id, &id, title, time, rating, director, overview, trailer, cover, backCover, tags, cast, popularity",
    tags: "&_id, &genre_id, genre",
    actors: "&_id, backCover, cover, followers, following, id, movies, name, overview",
    sync: "++id, type, data, token"
});

const CACHE_NAME = "gih-cache";
const BASE_STATIC_URLS = [
    "/",
    "/Home",
    "https://fonts.googleapis.com/icon?family=Material+Icons",
    "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
    "manifest.json",
    "/icon.png",
    "/img/Sheep.png",
    "/img/search.png",
    "/img/burger-menu.png",
    "/img/back-button.png",
    "/img/loginBackground.jpg",
    "/favicon.ico",
    "/dexie.js",
    "/serviceworker.js"
];

const STATIC_URLS = BASE_STATIC_URLS.concat(JSON.parse('%MANIFESTURLS%'));

self.addEventListener("install", function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME).then(function(cache) {
            return cache.addAll(STATIC_URLS);
        }).catch(error => {
            console.log(error);
        })
    );
});


self.addEventListener("sync", function(event) {
    if (event.tag === "message-queue-sync") {
        console.log("sync started")
        event.waitUntil(SyncMessages());
    }
});

self.addEventListener("fetch", function(event) {
    let url = event.request.url.replace(/^.*\/\/[^\/]+/, '');
    if (STATIC_URLS.filter(x => x.includes(url)).length > 0)
    {
        event.respondWith(caches.match(event.request));
    }
    else if (event.request.url.includes('/api/movie/tags')) {
        event.respondWith(
            fetch(event.request).catch(function (result) {
                return getTagsFromDb().then(function(response)
                {
                    var blob = new Blob([JSON.stringify(response, null, 2)], {type : 'application/json'});
                    var myResponse = new Response(blob);
                    return myResponse;
                });
            })
        );
    }
    else if (event.request.url.includes('/api/movie/tag/')) {
        event.respondWith(
            fetch(event.request).catch(function (result) {
                let searchArray = event.request.url.split('/');
                let tagid;
                if (searchArray.length > 0) {
                    tagid = searchArray[searchArray.length-1];
                }

                console.log(tagid)
                return getMoviesByTagFromDb(tagid).then(function(response)
                {
                    var blob = new Blob([JSON.stringify(response, null, 2)], {type : 'application/json'});
                    var myResponse = new Response(blob);
                    return myResponse;
                });

            })
        );
    }
    else if (event.request.url.includes('/api/movie/search')) {
        event.respondWith(
            fetch(event.request).catch(function (result) {
                let searchArray = event.request.url.split('/');
                let search;
                if (searchArray.length > 0) {
                    search = searchArray[searchArray.length-1];
                }

                console.log(search)
                return searchMoviesInDb(search).then(function(response)
                {
                    var blob = new Blob([JSON.stringify(response, null, 2)], {type : 'application/json'});
                    var myResponse = new Response(blob);
                    return myResponse;
                });

            })
        );
    }
    else if (event.request.url.includes('/api/actor/search')) {
        event.respondWith(
            fetch(event.request).catch(function (result) {
                let searchArray = event.request.url.split('/');
                let search;
                if (searchArray.length > 0) {
                    search = searchArray[searchArray.length-1];
                }

                console.log(search)
                return searchActorsInDb(search).then(function(response)
                {
                    var blob = new Blob([JSON.stringify(response, null, 2)], {type : 'application/json'});
                    var myResponse = new Response(blob);
                    return myResponse;
                });

            })
        );
    }
    else if (event.request.url.includes('/api/movie')) {
        event.respondWith(
            fetch(event.request).catch(function (result) {
                let idArray = event.request.url.split('/');
                let id;
                if (idArray.length > 0) {
                    id = idArray[idArray.length-1];
                }

                if (!isNaN(id))
                {
                    console.log(id)
                    return getMovieFromDb(id).then(function(response)
                    {
                        var blob = new Blob([JSON.stringify(response[0], null, 2)], {type : 'application/json'});
                        var myResponse = new Response(blob);
                        return myResponse;
                    });
                }
                else
                {
                    return getMoviesFromDb().then(function(response)
                    {
                        var blob = new Blob([JSON.stringify(response, null, 2)], {type : 'application/json'});
                        var myResponse = new Response(blob);
                        return myResponse;
                    });
                }
            })
        );
    }
    else if (event.request.url.includes('/api/actor')) {
        event.respondWith(
            fetch(event.request).catch(function (result) {
                let idArray = event.request.url.split('/');
                let id;
                if (idArray.length > 0) {
                    id = idArray[idArray.length-1];
                }

                console.log(id)
                return getActorFromDb(id).then(function(response)
                {
                    console.log(response)
                    var blob = new Blob([JSON.stringify(response[0], null, 2)], {type : 'application/json'});
                    var myResponse = new Response(blob);
                    return myResponse;
                });

            })
        );
    }
    else if(event.request.url.includes('/Movie/') ||
            event.request.url.includes('/Home') ||
            event.request.url.includes('/Login') ||
            event.request.url.includes('/Search') ||
            event.request.url.includes('/Messages') ||
            event.request.url.includes('/Settings') ||
            event.request.url.includes('/SettingsNotification') ||
            event.request.url.includes('/SettingsPassword') ||
            event.request.url.includes('/Actor') ||
            event.request.url.includes('/Following')
            )
    {
        event.respondWith(
            fetch(event.request).catch(function() {
                return caches.match("index.html").then(function(response) {
                    if (response) {
                        return response;
                    }
                });
            })
        );
    }
    else if (event.request.url.includes('/api/push/subscribe')) {
        let clone = event.request.clone();
        event.respondWith(
            fetch(event.request).catch(function (result) {
                clone.json().then(response => {
                    let token;
                    if (clone.headers.has('authorization'))
                    {
                        token = clone.headers.get('authorization');
                        token = token.replace('Bearer', '').trim()

                    }
                    saveSync("subscribe" ,response, token)
                })
                var blob = new Blob([JSON.stringify({msg: 'data saved to indexdb'}, null, 2)], {type: 'application/json'});
                var myResponse = new Response(blob);
                return myResponse;
            }
        ))
    }
    else if (event.request.url.includes('/api/push/unsubscribe')) {
        let clone = event.request.clone();
        event.respondWith(
            fetch(event.request).catch(function (result) {

                        let token;
                        if (clone.headers.has('authorization'))
                        {
                            token = clone.headers.get('authorization');
                            token = token.replace('Bearer', '').trim()

                        }
                        let searchArray = event.request.url.split('/');
                        let search;
                        if (searchArray.length > 0) {
                            search = searchArray[searchArray.length-1];
                        }

                        saveSync("unsubscribe", search, token)

                    var blob = new Blob([JSON.stringify({msg: 'data saved to indexdb'}, null, 2)], {type: 'application/json'});
                    var myResponse = new Response(blob);
                    return myResponse;
                }
            ))
    }
    else if (event.request.url.includes('/api/push/')) {
        console.log(event.request.url)
        event.respondWith(
            fetch(event.request).catch(function (result) {
                    var blob = new Blob([JSON.stringify({msg: 'data saved to indexdb'}, null, 2)], {type: 'application/json'});
                    var myResponse = new Response(blob);
                    console.log(myResponse)
                    return myResponse;
                }
            ))
    }
    else
    {
        console.log(event.request.url)
        event.respondWith(
            fetch(event.request).catch(function() {
                return caches.match(event.request).then(function(response) {
                    if (response) {
                        return response;
                    }
                });
            })
        );
    }
});


self.addEventListener('push', function (event) {
    const data = event.data.json();
    console.log("Getting push data", data);
    event.waitUntil(
        self.registration.showNotification(data.title, {
            body: data.msg,
            vibrate: [500, 100, 500],
            icon: "img/Sheep.png",
            data: data.id
        })
    );
});

//notification url redirect event click
self.addEventListener('notificationclick', function (event) {
    event.notification.close();

    event.waitUntil(
        clients.matchAll({
            type: "window"
        })
            .then(function (clientList) {
                if (clients.openWindow) {
                    return clients.openWindow("/Movie/" + event.notification.data);
                }
            })
    );
});

function getMoviesFromDb() {
    return new Promise((resolve, reject) => {
        db.movies.orderBy("popularity").desc().limit(10).toArray().then(movies => {
            resolve(movies);
        })
    });
}

function getMoviesByTagFromDb(id) {
    return new Promise((resolve, reject) => {
        db.movies.orderBy("popularity").desc().filter(x => x.tags.some(g => g.genreId === Number(id))).limit(10).toArray().then(movies => {
            resolve(movies);
        })
    });
}



function getMovieFromDb(id) {
    return new Promise((resolve, reject) => {
        db.movies.where("id").equals(Number(id)).toArray().then(movies => {
            resolve(movies);
        })
    });
}

function getActorFromDb(id) {
    return new Promise((resolve, reject) => {
        db.actors.where("id").equals(Number(id)).toArray().then(actor => {
            resolve(actor);
        })
    });
}

function getTagsFromDb() {
    return new Promise((resolve, reject) => {
        db.tags.toArray().then(tags => {
            resolve(tags);
        })
    });
}

function searchMoviesInDb(search) {
    return new Promise((resolve, reject) => {
        db.movies.filter(x => new RegExp(search, 'i').test(x.title)).toArray().then(movies => {
            resolve(movies);
        })
    });
}

function searchActorsInDb(search) {
    return new Promise((resolve, reject) => {
        db.actors.filter(x => new RegExp(search, 'i').test(x.name)).toArray().then(actors => {
            resolve(actors);
        })
    });
}

function saveSync(type, event, token) {

    db.transaction('rw', db.sync, async() => {
        db.sync.put({type: type, data: event, token: token});
    })
}

function getSyncQueueFromDb() {
    return new Promise((resolve, reject) => {
        db.sync.toArray().then(messages => {
            resolve(messages);
        })
    });
}

function deleteFromSyncQueue(id) {
    return new Promise((resolve, reject) => {
        db.sync.where('id').equals(id).delete().then(() => {
            resolve("item deleted");
        })
    });
}

function SyncMessages() {
        return getSyncQueueFromDb().then(function(messages) {
                return Promise.all(
                    messages.map(function (message) {
                        if (message.type === "subscribe")
                        {
                            console.log(message)
                            return fetch("/api/push/subscribe", {
                                method: "post",
                                headers : {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + message.token
                                },

                                body: JSON.stringify(message.data)
                            }).then(function () {
                                return deleteFromSyncQueue(message.id); // returns a promise
                            });
                        }
                        else if(message.type === "unsubscribe")
                        {
                            return fetch("/api/push/unsubscribe/" + message.data, {
                                method: "post",
                                headers : {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'Authorization': 'Bearer ' + message.token
                                }

                            }).then(function () {
                                return deleteFromSyncQueue(message.id); // returns a promise
                            });
                        }
                    })
                )
            }
        );
}
